//
//  RanchPlayer.h
//  Rancherita
//
//  Created by Iván Villamil Covián on 12/8/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "RCTBridgeModule.h"
#import <AVFoundation/AVFoundation.h>

@interface eRadioPlayer : NSObject <RCTBridgeModule>

@property(nonatomic) BOOL isPlaying;
@property(nonatomic, strong) AVPlayer *player;
@property(nonatomic, strong) AVPlayerItem *playerItem;

@end
