//
//  RanchPlayer.m
//  Rancherita
//
//  Created by Iván Villamil Covián on 12/8/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "eRadioPlayer.h"

@implementation eRadioPlayer

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(canPlay:(RCTResponseSenderBlock)callback)
{
  /*if (self.player == nil) {
    callback(@[@NO]);
  } else {
    BOOL canPlay = (self.player.status == AVPlayerStatusReadyToPlay);
    callback(@[@(canPlay)]);
  }*/
  if(self.playerItem.playbackLikelyToKeepUp){
    callback(@[@YES]);
  }else{
    callback(@[@NO]);
  }
}

RCT_EXPORT_METHOD(getVolume:(RCTResponseSenderBlock)callback)
{
  callback(@[@(self.player.volume)]);
}

RCT_EXPORT_METHOD(loadAudio:(NSString *)audioPath callback:(RCTResponseSenderBlock)callback)
{
  NSURL *audioURL = [NSURL URLWithString:audioPath];
  self.playerItem = [AVPlayerItem playerItemWithURL:audioURL];
  self.player = [[AVPlayer alloc] initWithPlayerItem:self.playerItem];
  self.isPlaying = NO;
  
  
  
  callback(@[]);
  
 // [self.player addObserver:self forKeyPath:@"status" options:0 context:nil];
  
  
}

/*- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
  if (object == self.player && [keyPath isEqualToString:@"status"]) {
    if (self.player.status == AVPlayerItemStatusReadyToPlay) {
      //playButton.enabled = YES;
      NSLog(@"Hello, World!");
      
    } else if (self.player.status == AVPlayerStatusFailed) {
      // something went wrong. player.error should contain some information
      NSLog(@"Hello, error al roca!");
    }
  }
}
*/

RCT_EXPORT_METHOD(play)
{
  [self.player play];
  self.isPlaying = YES;
}

RCT_EXPORT_METHOD(playing:(RCTResponseSenderBlock)callback)
{
  callback(@[@(self.isPlaying)]);
}

RCT_EXPORT_METHOD(pause)
{
  [self.player pause];
  [self.player cancelPendingPrerolls];
  self.player = nil;
  self.isPlaying = NO;
  self.playerItem = nil;
}

RCT_EXPORT_METHOD(setVolume:(float)volume)
{
  float validVolume = (volume > 1.0f) ? (volume/100.0f) : volume;
  if (self.player != nil) {
    [self.player setVolume:validVolume];
  }
}

@end
