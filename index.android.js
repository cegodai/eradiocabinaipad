import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';

import Main from './app/Main';
import store from './app/store';

class eRadioCabina extends Component {
    render() {
        return (
            <Provider store={store}>
                <Main/>
            </Provider>
        );
    }
}

AppRegistry.registerComponent('eRadioCabina', () => eRadioCabina);
