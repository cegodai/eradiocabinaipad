import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {post,pages,lists} from '../actions';
import PanelList from '../components/container/PanelList';

require("../../node_modules/moment/locale/es");

export default connect(
	(state) => ({
		post : state.post,
		pages : state.pages,
		lists : state.lists
    }),
    (dispatch) => bindActionCreators({...post,...pages,...lists}, dispatch)
)(PanelList);
