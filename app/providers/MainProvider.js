import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {session} from '../actions';
import Main from '../Main';

require("../../node_modules/moment/locale/es");

export default connect(
	(state) => ({
		session : state.session
    }),
    (dispatch) => bindActionCreators({...session}, dispatch)
)(Main);
