import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {post,pages,lists,post_list,settings} from '../actions';
import Panel from '../components/container/Panel';

require("../../node_modules/moment/locale/es");

export default connect(
	(state) => ({
		post : state.post,
		pages : state.pages,
		lists : state.lists,
		post_list : state.post_list,
		settings : state.settings
    }),
    (dispatch) => bindActionCreators({...post,...pages,...lists,...post_list,...settings}, dispatch)
)(Panel);
