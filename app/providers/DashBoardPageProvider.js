import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {post,pages,session,settings} from '../actions';
import DashboardPage from '../components/container/DashboardPage';

require("../../node_modules/moment/locale/es");

export default connect(
	(state) => ({
		session : state.session,
		post : state.post,
		pages : state.pages,
		settings : state.settings
    }),
    (dispatch) => bindActionCreators({...session,...post,...pages,...settings}, dispatch)
)(DashboardPage);
