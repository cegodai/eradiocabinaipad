import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {session} from '../actions';
import LoginPage from "../components/container/LoginPage";

export default connect(
	(state) => ({
        session : state.session
    }),
    (dispatch) => bindActionCreators({...session}, dispatch)
)(LoginPage);
