import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import NavPage from "../components/container/NavPage";
import actions from "../actions";

function selectProps(state) {
    return {...state}
}

function selectActions(dispatch) {
    return bindActionCreators({
        ...actions.menu,
        ...actions.pages,
        ...actions.session,
        ...actions.post,
        ...actions.lists,
        ...actions.post_list
    }, dispatch);
}

export default connect(selectProps, selectActions)(NavPage);
