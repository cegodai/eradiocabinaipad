import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {post,pages,lists,settings} from '../actions';
import PanelPosts from '../components/container/PanelPosts';

require("../../node_modules/moment/locale/es");

export default connect(
	(state) => ({
		post : state.post,
		pages : state.pages,
		lists : state.lists,
		settings : state.settings
    }),
    (dispatch) => bindActionCreators({...post,...pages,...lists,...settings}, dispatch)
)(PanelPosts);
