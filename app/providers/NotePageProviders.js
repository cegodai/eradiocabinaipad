import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {post,pages} from '../actions';
import NotePage from "../components/container/NotePage";

export default connect(
	(state) => ({
        post : state.post,
		pages : state.pages
    }),
    (dispatch) => bindActionCreators({...post,...pages}, dispatch)
)(NotePage);
