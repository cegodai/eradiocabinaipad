import React, {Component} from 'react';
import {
	View,
	Navigator,
    StyleSheet,
    StatusBar
} from 'react-native';

import {LoginPage} from './components'

export default class Main extends Component {

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle='light-content' />
                <Navigator
                    configureScene={(route) => {
						let animation = route.animation || 'FloatFromBottomAndroid';
						return Navigator.SceneConfigs[animation];
					}}
                    initialRoute={{name: 'Login', index: 0}}
                    ref="nav"
					renderScene={(route, navigator) => {
                        if (route.component) {
                            return <route.component
							 nav={navigator} {...route.props} />
                        }

                        return <LoginPage nav={navigator} />
                    }} />
            </View>
        );
    }

    _onPressBody(){
        this.props.unselectItem()
    }

}

const styles = StyleSheet.create({
    container : {
        flex : 1
    }
});
