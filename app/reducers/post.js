export default function(state = {}, action) {

	if(action.type === "FETCH_NEWS"){
		//console.log(action.collection)
		return {
			collection : action.collection.map((item,i) => {
				item.status = "new";
				item.assignedTo = undefined;
				return item;
			})
		}
	}

	if (action.type === "SET_ITEM_INDEX") {
        return {
			...state,
			index : action.index
        }
    }

    if (action.type === "VIEWING_NOTE") {
        const i = action.index;

        const copia = state.collection.map((item, i) => {
            item.isViewing = false;
            return item;
        })

        return [
            ...copia.slice(0, i),
            {
				...copia[i],
				isViewing : true
			},
            ...copia.slice(i + 1)
        ]
    }

    if (action.type === "CANCEL_VIEWING") {
        return state.collection.map((item, i) => {
            item.isViewing = false;
            return item;
        })
    }

    if(action.type === "UNSELECT_ALL_ITEMS"){
    	return {
			...state,
			index : -1
		}
    }

    if(action.type === 'ASSING_POST_TO'){

		let postId = action.id;

		let newCollection = state.collection.map((item,i) => {
			if(item.id === postId){
				item.assignedTo = action.to;
				item.status = "assined";
			}
			return item;
		});

        return {
			...state,
			collection : newCollection
		};
    }


    return state;
}
