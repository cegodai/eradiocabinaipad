export default function (state = {}, action) {
    if (action.type === "LOGIN") {
        return {
            ...state,
            active: true
        };
    }

    if (action.type === "LOGOUT") {
        return {
            ...state,
            active: false
        };
    }

    if (action.type === "UPDATE_USER_VALUE") {
        if (undefined !== state.user[action.key]) {
            return {
                ...state,
                user: {
                    ...state.user,
                    [action.key] : action.value
                }
            }
        }
    }

    return state;
}
