import {combineReducers} from "redux";

import menu from "./menu";
import pages from "./pages";
import session from "./session";
import post from "./post";
import lists from './lists';
import post_list from './post_list';
import settings from './settings';

const reducers = combineReducers({
    menu,
    pages,
    session,
    post,
    lists,
    post_list,
	settings
});

export default reducers;
