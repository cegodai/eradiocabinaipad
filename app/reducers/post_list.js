export default function(state = {}, action) {

	if(action.type === "MOVE_TO_LIST"){
        let postListId = Math.random().toString(36).substr(2, 10)
        //console.log(state,'antes')
        return [
            ...state,
            {
                id : postListId,
                postID : action.id,
                listID : action.list
            }
        ];
    }

    return state

}
