export default function(state = {}, action) {

    if (action.type === "LOGIN") {
        return {
            ...state,
            previousPage: state.activePage,
            activePage: "DashBoardPage"
        }
    }

    if (action.type === "LOGOUT") {
        return {
            ...state,
            previousPage: state.activePage,
            activePage: "Login"
        }
    }

    if(action.type === "SET_NOTE_INDEX"){
        return {
            ...state,
            currentNote : action.postID
        }
    }

    if (action.type === "UPDATE_PAGE") {
        return {
            ...state,
            previousPage: state.activePage,
            activePage: action.page
        }
    }

    if (action.type === "UPDATE_PENDING_PAGE") {
        return {
            ...state,
            pendingPage: action.page
        }
    }

    return state;
}
