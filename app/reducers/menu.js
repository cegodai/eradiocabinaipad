export default function(state = {}, action) {
    if (action.type === "CHANGE_INDEX") {
        return {
            ...state,
            index: action.index
        }
    }

    if (action.type === "OPEN_MENU") {
        return {
            ...state,
            isOpen: !state.isOpen
        }
    }


    if(action.type === "SHOW_OVERLAY"){
        return {
            ...state,
            showOverlay : !state.showOverlay
        }
    }


    if (action.type === "LOGOUT") {
        return {
            ...state,
            index: 0
        }
    }



    return state;
}
