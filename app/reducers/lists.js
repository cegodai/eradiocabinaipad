export default function(state = {}, action) {
    if (action.type === "ADD_LIST") {
        let newId = Math.random().toString(36).substr(2, 10)
        return [
            ...state,
            {
                id: newId,
                name: action.name
            }
        ];
    }

    if(action.type === "CHANGE_INDEX_LIST"){
        const i = action.index
        return [
            ...state.slice(0, i),
            {...state[i], isSelected : true},
            ...state.slice(i + 1)
        ]
    }

    if(action.type === "UNSELECT_ALL_LISTS"){
        return state.map(item => {
            item.isSelected = false;
            return item;
        })
    }

    return state;
}
