export default function(state = {}, action) {

	if(action.type === "SET_TAB_INDEX_POSTS"){
		return {
			...state,
			selectedList : action.index
		}
	}

	if(action.type === "SET_CONTENT_TYPE"){
		return {
			...state,
			contentType : action.value
		}
	}

	return state;

}
