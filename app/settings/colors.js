module.exports ={
	text : "#000000",
	blue : '#157594',
	blueBold : '#0A5B76',
	orange : '#ED861A',
	white : '#ffffff',
	black : '#333333',
	grey : '#EFEFEF',
	greyBold : '#95989A',
	orangeLight : '#DA9328',
	green : '#13AD47',
	greysemiLight : '#CCCCCC',
	greysemiBold : '#777777',
	semiGrey : '#FAFAFA'
}
