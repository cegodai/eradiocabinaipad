import {eRadioPlayer} from 'NativeModules';

export function load(audioPath,fn){
	if(audioPath != ""){
		eRadioPlayer.loadAudio(audioPath,(response) => {
			fn(response);
		});
	}
}

export function canPlay(fn){
	eRadioPlayer.canPlay((response) => {
		fn(response);
	});
}

export function isPlaying(fn){
	eRadioPlayer.playing((response) => {
		fn(response);
	});
}

export function play(){
	eRadioPlayer.play()
}


export function pause(){
	eRadioPlayer.pause()
}
