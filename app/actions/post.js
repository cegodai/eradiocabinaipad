import posts from "../data/posts";

export function fetchNews(){
	return {
		type : "FETCH_NEWS",
		collection : posts
	}
}

export function setItemIndex(index) {
    return {
        type: "SET_ITEM_INDEX",
        index
    }
}

export function unselectItem(){
	return {
		type : "UNSELECT_ALL_ITEMS"
	}
}

export function assignPostTo(id,to){
	return {
		type : "ASSING_POST_TO",
		id,
		to
	}
}

export function setView(index){
	return {
		type : "VIEWING_NOTE",
		index
	}
}

export function cancelView(){
	return {
		type : "CANCEL_VIEWING"
	}
}
