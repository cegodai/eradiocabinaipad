export function changeIndex(index) {
    return {
        type: "CHANGE_INDEX",
        index
    }
}

export function openMenu() {
    return {
        type: "OPEN_MENU"
    }
}

export function showOverlay(){
    return {
        type : "SHOW_OVERLAY"
    }
}

export function logout(){
	return {
		type : "LOGOUT"
	}
}