export function addListToPanel(name) {
    return {
        type: "ADD_LIST",
        name
    }
}

export function selectList(index){
	return {
		type : "CHANGE_INDEX_LIST",
		index
	}
}

export function unselectAllLists(){
	return {
		type : "UNSELECT_ALL_LISTS"
	}
}
