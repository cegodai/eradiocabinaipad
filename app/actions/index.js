import * as menu from "./menu";
import * as pages from "./pages";
import * as session from "./session";
import * as post from "./post";
import * as lists from './lists';
import * as post_list from './post_list';
import * as settings from './settings';


module.exports = {
	pages : {...pages},
	session : {...session},
	settings : {...settings},
    menu: {...menu},
    post : {...post},
    lists : {...lists},
    post_list : {...post_list}
};
