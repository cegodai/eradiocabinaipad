export function login(credentials) {
    return {
        type: "LOGIN",
        credentials
    }
}

export function logout() {
    return {
        type: "LOGOUT"
    }
}

export function updateUserValue(key, value) {
    return {
        type: "UPDATE_USER_VALUE",
        key,
        value
    }
}