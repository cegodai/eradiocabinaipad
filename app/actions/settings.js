export function setTab(index){
	return {
		type : "SET_TAB_INDEX_POSTS",
		index
	}
}

export function setContentType(value){
	return {
		type : "SET_CONTENT_TYPE",
		value
	}
}
