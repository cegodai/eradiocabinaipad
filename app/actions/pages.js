export function updatePage(page) {
    return {
        type: "UPDATE_PAGE",
        page
    }
}

export function updatePendingPage(page) {
    return {
        type: "UPDATE_PENDING_PAGE",
        page
    }
}

export function setNotePageDetails(postID){
	return {
		type : "SET_NOTE_INDEX",
		postID
	}
}