import thunkMiddleware from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import createLogger from 'redux-logger';
import reducers from './reducers';


const loggerMiddleware = createLogger();
const defaultStore = {
    menu: {
        index: 0,
        isOpen: false
    },
    session : {
    	active : 0
    },
    lists : [],
	settings : {
		selectedList : 0,
		contentType : 'news'
	},
    post : {
		collection : [],
		index : ""//es el id de la nota seleccionada
	},
	pages : {
		currentNote : 0
	}
};

const store = createStore(reducers, defaultStore, applyMiddleware(thunkMiddleware, loggerMiddleware));
export default store;
