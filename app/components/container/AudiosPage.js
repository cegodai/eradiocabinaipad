import React,{Component} from 'react';
import {
	View,
	Text,
	ScrollView,
	StyleSheet
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../settings';
import AudioItem from '../presentation/AudioItem';
import {Sound} from '../../helpers';

class AudiosPage extends React.Component {

	constructor(){
		super();
		this.state = {
			audioIndex : -1,
			indexPlaying : -1
		};

		this.timeout = null;
	}

    render() {
        return (
        	<View style={styles.container}>
        		<View style={styles.marginLeft}>
        			<Icon name="microphone" size={35} color={colors.greyBold} />
        			<Text style={styles.title}>Audios</Text>
        		</View>
        		<ScrollView>
        			{this._renderAudios()}
        		</ScrollView>
        	</View>
        );
    }

	_canPlay(index){
		Sound.canPlay((response) => {

			if(this.timeout){
				clearTimeout(this.timeout);
			}

			if(response === false){
				this.timeout = setTimeout(() => {
					this.setState({
						audioIndex : index,
						indexPlaying : -1
					});
					this._canPlay(index);
				},500);
			}else{
				this.timeout = setTimeout(() => {
					this.setState({
						audioIndex : -1,
						indexPlaying : index
					});
					Sound.play();
				},500);
			}
		})
	}

	_playSound(soundPath,index){
		Sound.isPlaying((response) => {
			if(response){
				Sound.pause();
				this.setState({
					audioIndex : -1,
					indexPlaying : -1
				});
			}else{
				Sound.load(soundPath,(response) => {
					this._canPlay(index);
				});
			}
		});
	}

    _renderAudios(){
		const {pages,post} = this.props;
    	let postID = pages.currentNote
    	let posts = post.collection.find((item) => item.id == postID)

    	return posts.audios.map((item,i) =>
    		<AudioItem
				key={i}
				src={item.uri}
				duration="00:30"
				name={item.name}
				id={item.id}
				index={i}
				indexPlaying={this.state.indexPlaying}
				isLoading={this.state.audioIndex}
				onPress={() => {
					this._playSound(item.uri,i)
				}}
			/>
    	)
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		paddingTop : 20
	},
	marginLeft : {
		flexDirection : 'row',
		paddingLeft : 20,
		alignItems : 'center',
		justifyContent : 'flex-start',
		paddingBottom : 20
	},
	title : {
		fontSize : 30,
		color : colors.greyBold,
		paddingLeft : 20,
	}
})

export default AudiosPage;
