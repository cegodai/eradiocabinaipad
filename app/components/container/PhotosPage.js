import React,{Component} from 'react';
import {
	Text,
	View,
	ScrollView,
	StyleSheet,
	Dimensions,
	Modal
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../settings';
import PhotoItem from '../presentation/PhotoItem';
import {ImageViewer} from '../';

const screen = Dimensions.get('window')

class PhotosPage extends Component {

	constructor(){
		super();
		this.state = {
			modalVisible : false,
			modalIndex : -1
		};
	}

    render() {
        return (
        	<View style={styles.container}>
        		<View style={styles.marginLeft}>
        			<Icon name="camera" size={35} color={colors.greyBold} />
        			<Text style={styles.title}>Fotos</Text>
        		</View>
        		<ScrollView
					style={styles.scrollView}
        			contentContainerStyle={styles.scrollContainer}
        		>
        			{this._renderPhotos()}
        		</ScrollView>

				<Modal
		          animationType={"slide"}
		          transparent={true}
		          visible={this.state.modalVisible}
		          onRequestClose={() => {}}
		          >

					<ImageViewer onClose={() => {
						this._toggleModal(-1)
					}} collection={this._getPhotosCollection()}
					index={this.state.modalIndex} />
				</Modal>
        	</View>
        )
    }

	_getPhotosCollection(){
		const {post,pages} = this.props;
    	//obtener las fotos de la nota por medio de redux
    	let postID = pages.currentNote;
    	let posts = post.collection.find((item) => item.id == postID);

		return posts.photos;
	}

	_toggleModal(indexPhoto){
		//alert(indexPhoto)
		this.setState({
			modalVisible : !this.state.modalVisible,
			modalIndex : indexPhoto
		});
	}

    _renderPhotos(){
		const {post,pages} = this.props;
    	//obtener las fotos de la nota por medio de redux
    	let postID = pages.currentNote
    	let posts = post.collection.find((item) => item.id == postID)

    	return posts.photos.map((item,i) => {
			return (
				<PhotoItem
					key={i}
					src={item.uri}
					name={item.name}
					id={item.id}
					onPress={() => {
						 this._toggleModal(i);
					}}
				/>
			);
		});
    }
}

const styles = StyleSheet.create({
	container : {
		paddingTop: 20,
		flex : 1
	},
	scrollView : {
		marginTop : 20
	},
	scrollContainer : {
		flexDirection : 'row',
		flexWrap : 'wrap',
		padding : 20
	},
	marginLeft : {
		flexDirection : 'row',
		paddingLeft : 20,
		alignItems : 'center',
		justifyContent : 'flex-start'
	},
	title : {
		fontSize : 30,
		color : colors.greyBold,
		paddingLeft : 20,
	}
})

export default PhotosPage;
