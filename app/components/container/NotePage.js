import React,{Component} from 'react';
import {
	View,
	Text,
	ScrollView,
	StyleSheet,
	TouchableOpacity,
	Dimensions,
	Animated,
	InteractionManager
} from 'react-native';

import {colors} from '../../settings';
import {
	Icon, PhotosPage, VideosPage, AudiosPage, NoteContent
} from '../';

const screen = Dimensions.get('window');

class NotePage extends Component {
    constructor(){
    	super()

    	this.state = {
    		isMenuOpen : false,
    		activePage : 'note'
    	};

		this.menu = new Animated.Value(-150);
		this.container = new Animated.Value(0)
    }

    _openMenu(){
    	this.setState({
    		isMenuOpen : !this.state.isMenuOpen
    	})

		InteractionManager.runAfterInteractions(() => {
			Animated.spring(this.menu,{
				toValue : (this.state.isMenuOpen) ? 0 : -150
			}).start();

			Animated.spring(this.container,{
				toValue : (this.state.isMenuOpen) ? 150 : 0
			}).start();
		});
    }

    _setActivePage(page){
    	this.setState({
    		activePage : page
    	})
    }

    render() {
		const {post,nav} = this.props;
		const htmlContent = this._getNoteContent(post.index);
        return (
        	<View style={styles.container}>
        		<View style={styles.header}>
        			<Text style={styles.headerText}>Noticia</Text>
        		</View>

        		<Animated.View  style={[{marginRight : this.container},styles.subheader]}>


					<TouchableOpacity
						activeOpacity={0.4}
						onPress={() => {
							this._onPressBack();
						}}
					>
						<Icon name="arrow-left" size={30} color={colors.greyBold} />
					</TouchableOpacity>


					<TouchableOpacity
	    				activeOpacity={0.4}
	    				onPress={() => {
							this._openMenu()
						}}
						style={{paddingLeft : 10,paddingRight : 10}}
	    			>
	    				<Icon name={(this.state.isMenuOpen) ? "times" : "ellipsis-v"} size={23} color={colors.greyBold} />
	    			</TouchableOpacity>

        		</Animated.View>


        		<Animated.View style={[{marginRight:this.container},styles.noteContainer]}>
        			{(() => {
        				if(this.state.activePage == 'note'){
        					return (
        						<NoteContent
									nav={nav}
									htmlContent={htmlContent.note}
									title={htmlContent.title}
								 />
        					)
        				}else if(this.state.activePage == 'photos'){
        					return (
        						<PhotosPage {...this.props} />
        					)
        				}else if(this.state.activePage == 'videos'){
        					return (
        						<VideosPage {...this.props} />
        					)
        				}
        				else if(this.state.activePage == 'audios'){
        					return (
        						<AudiosPage {...this.props} />
        					)
        				}
        			})()}
        		</Animated.View>

				<Animated.View style={[{right : this.menu},styles.rightMenu]}>
        			<TouchableOpacity
        				activeOpacity={0.4}
        				style={styles.itemMenu}
        				onPress={() => {
							this._setActivePage('note');
						}}
        			>
        				<Icon name="pencil" size={40} color={colors.white} />
        				<Text style={styles.itemMenuText}>Nota</Text>
        			</TouchableOpacity>
        			<TouchableOpacity
        				activeOpacity={0.4}
        				style={styles.itemMenu}
        				onPress={() => {
							this._setActivePage('photos');
						}}
        			>
        				<Icon name="camera" size={40} color={colors.white} />
        				<Text style={styles.itemMenuText}>Fotos</Text>
        			</TouchableOpacity>
        			<TouchableOpacity
        				activeOpacity={0.4}
        				style={styles.itemMenu}
        				onPress={() => {
							this._setActivePage('videos');
						}}
        			>
        				<Icon name="video-camera" size={40} color={colors.white} />
        				<Text style={styles.itemMenuText}>Videos</Text>
        			</TouchableOpacity>
        			<TouchableOpacity
        				activeOpacity={0.4}
        				style={styles.itemMenu}
        				onPress={() => {
							this._setActivePage('audios');
						}}
        			>
        				<Icon name="microphone" size={40} color={colors.white} />
        				<Text style={styles.itemMenuText}>Audios</Text>
        			</TouchableOpacity>
        			<TouchableOpacity
        				activeOpacity={0.4}
        				style={styles.itemMenuGreen}
						onPress={() => {
							//añadir a la lista seleccionada
							//this._onSelectItem(htmlContent.id);
						}}
        			>
        				<Icon name="plus" size={40} color={colors.white} />
        				<Text style={styles.itemMenuText}>Agregar a lista</Text>
        			</TouchableOpacity>
        		</Animated.View>
        	</View>
        );
    }

	_getNoteContent(index){
		console.log(index,'esto devuelve al entrar a la nota')
		const {post} = this.props;

		let note = post.collection.find((item) => {
			if(item.id === index){
				return item
			}
		})//[index];

		return note;
	}

    _onPressBack(){
		const {setNotePageDetails} = this.props;
    	setNotePageDetails(undefined)
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 2,
		flexDirection : 'column',
		position : 'relative'
	},
	header : {
		flexDirection : 'row',
		alignItems : 'center',
		justifyContent : 'center',
		backgroundColor : colors.blueBold,
		height : 45
	},
	subheader : {
		flexDirection : 'row',
		alignItems : 'center',
		justifyContent : 'space-between',
		backgroundColor : colors.grey,
		height : 50,
		paddingLeft : 20,
		paddingRight : 20
	},
	headerText : {
		color : colors.white,
		fontSize : 18
	},
	rightMenu : {
		flex : 1,
		backgroundColor : '#157594',
		position : 'absolute',
		top : 0,
		bottom : 0,
		width : 150,
		zIndex : 9
	},
	itemMenu : {
		flex : 1,
		flexDirection : 'column',
		alignItems : 'center',
		justifyContent : 'center',
		alignSelf : 'center',
		padding : 20
	},
	itemMenuGreen : {
		flex : 1,
		flexDirection : 'column',
		alignItems : 'center',
		justifyContent : 'center',
		alignSelf : 'center',
		backgroundColor : colors.green,
		padding : 20
	},
	itemMenuText : {
		color : colors.white,
		fontSize : 17,
		alignSelf : 'center',
		textAlign : 'center',
		paddingTop : 15
	},
	openMenu : {
		right : 0
	},
	hideMenu : {
		right : -150
	},
	moveContainer : {
		marginRight : -150
	},
	backContainer : {
		marginRight : 0
	},
	noteContainer : {
		flex:1,
		backgroundColor : colors.white
	}
})

export default NotePage;
