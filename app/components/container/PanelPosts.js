import React,{Component} from 'react';
import {
	View,
	StyleSheet,
	Text,
	ScrollView
} from 'react-native'

import {colors} from '../../settings';
import PostItem from '../presentation/PostItem';
import {Utils} from '../../helpers';

class PanelPosts extends Component {
    render() {
		const {lists} = this.props;

    	const arrayList = lists;
    	let list = undefined
    	if(undefined != arrayList && arrayList.length > 0){
    		list = arrayList.find(item => item.isSelected == true)
    	}

        return (
        	<View style={styles.container}>
        		<View style={styles.header}>
        			<Text style={styles.headerTitle}>
        				Noticias de la lista
        			</Text>
        		</View>

        		{(() => {
        			if(undefined != list){
        				return (
        					<View style={{flex:1}}>
        						<View style={styles.subHeader}>
        							<Text
        								style={styles.subHeaderText}
        								numberOfLines={1}
        							>{list.name}</Text>
        						</View>
        						<ScrollView>
        							{this._renderPostsList()}
        						</ScrollView>
        					</View>
        				)
        			}else{
        				return (
        					<View style={styles.content}>
			        			<Text
				        			numberOfLines={2}
				        			style={styles.textOverlay}
				        		>
				        			Selecciona una lista para
				        			visualizar las notas
				        		</Text>
			        		</View>
        				)
        			}
        		})()}

        	</View>
        )
    }

    _renderPostsList(){
		const {
			lists,
			post,
			setContentType,
			setNotePageDetails,
			setItemIndex,
			unselectItem,
			pages
		} = this.props;

    	let list = lists.find((item) => item.isSelected)
    	let posts = post.collection.filter((post) => {
			return post.assignedTo == list.id;
		});

    	//obtener los posts asociados a la lista seleccionada
    	if(undefined != posts && posts.length > 0){
	    	//console.log(posts)
	    	return posts.map((item,i) => {
	    		//buscar el elemento de la lista por el id
		    		return (
						<PostItem
							key={i}
							id={item.id}
							desc={Utils.striptags(item.title)}
							image={(item.photos.length > 0) ? item.photos[0].uri : ""}
							author={item.author}
							onPress={() => {
								unselectItem();
								setItemIndex(item.id);
								setNotePageDetails(item.id);
								setContentType("fromLists");
							}}
							currentPage={pages.currentNote}
						/>
					)
	    		}
	    	)

    	}
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		backgroundColor : colors.white
	},
	content : {
		flex : 1,
		backgroundColor : colors.grey,
		alignItems : 'center',
		justifyContent : 'center'
	},
	textOverlay : {
		color : colors.greyBold,
		fontSize : 20,
		textAlign : 'center'
	},
	header : {
		backgroundColor : colors.blueBold,
		height : 45,
		alignItems : 'center',
		justifyContent : 'center'
	},
	headerTitle : {
		color : colors.white,
		fontSize : 18
	},
	subHeader : {
		backgroundColor : colors.grey,
		height : 50,
		alignItems : 'center',
		justifyContent : 'center'
	},
	subHeaderText : {
		fontSize : 16,
		alignSelf :'flex-start',
		color : colors.greyBold,
		paddingLeft : 25
	}
})

export default PanelPosts;
