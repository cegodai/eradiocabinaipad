import React,{Component} from 'react';
import {
	View,
	Text,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../settings';
import {Utils} from '../../helpers';

import WebViewAutoHeight from '../presentation/WebViewAutoHeight';
import NoteEditionPage from '../NoteEditionPage';

const ITEM_PADDING = 10;

class NoteContent extends Component {

    render() {
		const {htmlContent,title,nav} = this.props;
        return (
        	<View style={styles.container}>
        
				<View style={styles.marginLeft}>
        			<Icon name="pencil" size={35} color={colors.greyBold} />
        			<Text style={styles.title}>Nota</Text>
        		</View>

				<ScrollView contentContainerStyle={styles.scrollContainer}>

					<TouchableOpacity onPress={() => {
						nav.push({
							component : NoteEditionPage,
							props : {
								title,
								htmlContent
							}
						});
					}}>

						<Text style={styles.noteTitle}>
							{Utils.striptags(title)}
						</Text>

						<WebViewAutoHeight
							source={{html : "<body>"+htmlContent+"</body>"}}
						/>

					</TouchableOpacity>
				</ScrollView>


        	</View>
        )
    }

}

const styles = StyleSheet.create({
	container : {
		flex : 1
	},
	scrollView: {
		marginTop: 0
	},
	scrollContainer : {
		padding : 20
	},
	editBar : {
		backgroundColor : colors.semiGrey,
		height : 45,
		flexDirection : 'row',
		alignItems : "center",
		justifyContent : "space-between",
		paddingLeft : 20,
		paddingRight : 20
	},
	marginLeft : {
		flexDirection : 'row',
		paddingLeft : 20,
		alignItems : 'center',
		justifyContent : 'flex-start'
	},
	title : {
		fontSize : 30,
		color : colors.greyBold,
		padding : 20,
	},
	noteTitle : {
		fontSize : 25,
		color : colors.greyBold,
		marginBottom : 20
	},
	textStyle : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between"
	},
	alignTexts : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between"
	},
	listStyle : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between"
	},
	btnPadding : {
		paddingLeft : ITEM_PADDING,
		paddingRight : ITEM_PADDING
	},
	titleSelector : {
		flex : 1,
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between",
		paddingLeft : ITEM_PADDING,
		paddingRight : ITEM_PADDING
	}
})

export default NoteContent;
