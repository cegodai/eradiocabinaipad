import React,{Component} from 'react';
import {
	View,
	Text,
	StyleSheet
} from 'react-native'

import {colors} from '../../settings';
import {NotePage,Panel,PanelList,PanelPosts} from '../';
import Header from '../presentation/Header';

class DashboardPage extends Component {
    render() {
		const {nav,settings,pages} = this.props;
		console.log(settings)
        return (
        	<View style={styles.container}>

				<Header />

				<View style={styles.content}>

	                {(() => {
	                    let current = pages.currentNote;
	                    if(undefined != current && current != '' && settings.contentType == "news"){
	                        return (
	                            <View style={styles.content}>
	                                <Panel {...this.props} type="noticias" title="Noticias" />
	                                <NotePage nav={nav} />
	                            </View>
	                        )
	                    }else if(undefined != current && current != '' && settings.contentType == "fromLists"){
	                        return (
	                            <View style={styles.content}>
	                                <PanelPosts />
	                                <NotePage nav={nav} />
	                            </View>
	                        )
	                    }else{
	                        return (
	                            <View style={styles.content}>
	                                <Panel type="noticias" title="Noticias" />
	                                <PanelList />
	                                <PanelPosts />
	                            </View>
	                        )
	                    }
	                })()}

				</View>


        	</View>
        );
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		backgroundColor : colors.grey
	},
	content : {
		flex : 1,
		flexDirection : 'row'
	}
})

export default DashboardPage;
