import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	ScrollView,
	TouchableOpacity,
	Dimensions,
	InteractionManager
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../settings';
import PanelItem from '../presentation/PanelItem'

import {Utils} from '../../helpers';

const screen = Dimensions.get('window')
const ITEM_HEIGHT = screen.width / 7;

import {ViewPager} from '../';

class Panel extends Component {

	componentDidMount(){
		const {post,fetchNews} = this.props;

		if(post.collection.length === 0){
			fetchNews();
		}
	}

    render() {
    	const {title,type,settings} = this.props
        return (
        	<View style={styles.container}>
        		<View style={styles.header}>
        			<Text style={styles.headerTitle}>{title}</Text>
        		</View>

        		<View style={styles.subHeader}>
        			<TouchableOpacity
        				activeOpacity={0.4}
        				style={[styles.tab,(settings.selectedList === 0) ? styles.tabActive : {}]}
        				onPress={() => {
							this._changeListIndex(0);
						}}
        			>
        				<Text style={[styles.tabText,(settings.selectedList === 0) ? styles.tabTextActive : {}]}>Nuevas</Text>
        			</TouchableOpacity>
					<TouchableOpacity
        				activeOpacity={0.4}
        				style={[styles.tab,(settings.selectedList === 1) ? styles.tabActive : {}]}
        				onPress={() => {
							this._changeListIndex(1);
						}}
        			>
        				<Text style={[styles.tabText,(settings.selectedList === 1) ? styles.tabTextActive : {}]}>Sin asignar</Text>
        			</TouchableOpacity>
					<TouchableOpacity
        				activeOpacity={0.4}
        				style={[styles.tab,(settings.selectedList === 2) ? styles.tabActive : {}]}
        				onPress={() => {
							this._changeListIndex(2);
						}}
        			>
        				<Text style={[styles.tabText,(settings.selectedList === 2) ? styles.tabTextActive : {}]}>Todas</Text>
        			</TouchableOpacity>
        		</View>

        		<ViewPager
        			style={styles.scrollContainer}
					selectedIndex={settings.selectedList}
					onSelectedIndexChange={(index) => {
						this._changeListIndex(index);
					}}
				>
					<ScrollView style={styles.page}>
						{this._filterBy('news').length > 0 && this._filterBy('news')}
						{this._filterBy('news').length === 0 && (
							<View style={styles.notFoundNotes}>
								<Text>Sin notas nuevas</Text>
							</View>
						)}
					</ScrollView>
					<ScrollView style={styles.page}>
						{this._filterBy('noAssinged').length > 0 && this._filterBy('noAssinged')}
						{this._filterBy('noAssinged').length === 0 && (
							<View style={styles.notFoundNotes}>
								<Text>No hay notas sin asignar</Text>
							</View>
						)}
					</ScrollView>
					<ScrollView style={styles.page}>
						{this._filterBy('all').length > 0 && this._filterBy('all')}
						{this._filterBy('all').length === 0 && (
							<View style={styles.notFoundNotes}>
								<Text>No hay notas</Text>
							</View>
						)}
					</ScrollView>
				</ViewPager>

        	</View>
        )
    }

	_changeListIndex(index){
		const {setTab} = this.props;
		setTab(index);
	}

    _filterBy(action){
		const {post} = this.props;
		let posts = [];

		//los posts deben venir ordenados por fecha...

    	switch(action){
    		case "news":
    			posts = post.collection.filter((item, i) => {
					return item.status == "new";
				});
    		break;

			case "all":
				posts = post.collection.map((item,i) => {
					return item;
				});
			break;

    		case "noAssinged":
    			posts = post.collection.filter((item) => {
					return item.assignedTo == undefined || item.assignedTo == '';
				});
    		break;
    	}

		return posts.map((item,i) => {
			return (
				<PanelItem {...this.props}
					key={i}
					onPress={() => {this._onPressItem(item.id);}}
					desc={item.title}
					assignedTo={this._getListName(item.assignedTo)}
					image={(item.photos.length > 0) ? item.photos[0].uri : ""}
					author={item.author}
					itemIndex={post.index}
					index={item.id}
					style={(post.index == item.id) ? styles.selected : {} }
				>
					{this._renderVibrantContent(item,i)}
				</PanelItem>
			);
		});
    }

	_getListName(id){
		const {post_list,lists} = this.props;

		if(!Utils._isEmptyObject(post_list) && id != undefined){
			let list = lists.find((listItem) => listItem.id == id);
			return list.name;
		}else{
			return '';
		}
	}

    _renderVibrantContent(item,index){
    	return (
    		<View style={styles.overlay}>
    			<TouchableOpacity
    				activeOpacity={0.4}
    				onPress={() => {
						this._onPressDetails(item.id);
					}}
    				>
	    			<View style={styles.btnLeft}>
	    				<Text style={{textAlign:'center'}}>
	    					<Icon name="search" size={30} color={colors.white} />
	    				</Text>
	    			</View>
	    			<Text style={{color:colors.orangeLight,textAlign:'center'}}>Detalles</Text>
    			</TouchableOpacity>
    			<TouchableOpacity
    				activeOpacity={0.4}
    				onPress={() => {
						this._onSelectItem(item,index)
					}}>
	    			<View style={styles.btnRight}>
	    				<Text style={{textAlign:'center'}}>
	    					<Icon name="plus" size={30} color={colors.white} />
	    				</Text>
	    			</View>
	    			<Text style={{color:colors.green,textAlign:'center'}}>Agregar a la lista</Text>
    			</TouchableOpacity>
    		</View>
    	)
    }

    _onPressDetails(postID){
		const {setNotePageDetails,setContentType} = this.props;
    	setNotePageDetails(postID);
		setContentType("news");
    }

    _onPressItem(index){
		const {setItemIndex} = this.props;
    	setItemIndex(index);
    }

    _onSelectItem(item,index){

		const {lists,moveToList,assignPostTo,unselectItem} = this.props;

    	if(undefined != lists && lists.length > 0){
	    	let list = lists.find((el) => el.isSelected)

	    	if(undefined != list && list != ''){
	    		moveToList(list.id,item.id)
	    		assignPostTo(item.id,list.id)
	    		unselectItem();
	    	}
	    }

    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		backgroundColor : colors.white
	},
	scrollContainer : {
		borderColor : colors.grey,
		borderRightWidth : 2
	},
	subHeader : {
		backgroundColor : colors.grey,
		height : 50,
		alignItems : 'center',
		justifyContent : 'space-between',
		borderWidth : 0,
		flexDirection : 'row',
		shadowColor : colors.grey,
		shadowOpacity : 0.5,
		shadowOffset : {
			width : 3,
			height : 3
		},
		zIndex : 2
	},
	tab : {
		backgroundColor : 'transparent',
		flex : 1,
		height : 50,
		alignItems : 'center',
		justifyContent : 'center',
	},
	tabActive : {
		borderBottomWidth : 2,
		borderBottomColor : colors.blue
	},
	tabTextActive : {
		color : colors.blue
	},
	tabContent : {
		flex : 1,
		flexDirection : 'column'
	},
	tabText : {
		fontSize : 15,
		alignSelf :'center',
		color : colors.greyBold
	},
	header : {
		backgroundColor : colors.blueBold,
		height : 45,
		alignItems : 'center',
		justifyContent : 'center',
		borderWidth : 0
	},
	headerTitle : {
		color : colors.white,
		fontSize : 18
	},
	selected : {
		backgroundColor : 'rgba(22,116,147,.1)'
	},
	blurSection : {
		flex:1,
		backgroundColor : 'rgba(22,116,147,.1)',
		//height : ITEM_HEIGHT
	},
	overlay : {
		flex : 1,
		flexDirection : 'row',
		alignItems : 'center',
		justifyContent : 'space-around',
		//height : ITEM_HEIGHT
	},
	btnLeft : {
		alignSelf : 'center',
		borderRadius : 100,
		backgroundColor : colors.orangeLight,
		padding : 18,
		shadowColor : colors.greyBold,
		shadowOpacity : 0.5,
		shadowOffset : {
			width : 3,
			height : 3
		},
		width : 70,
		height : 70
	},
	btnRight : {
		alignSelf : 'center',
		borderRadius : 100,
		backgroundColor : colors.green,
		padding : 18,
		shadowColor : colors.greyBold,
		shadowOpacity : 0.5,
		shadowOffset : {
			width : 3,
			height : 3
		},
		width : 70,
		height : 70
	},
	page : {
		flex : 1
	},
	notFoundNotes : {
		flex : 1,
		padding : 20,
		alignItems : "center",
		justifyContent : "center"
	}
})

export default Panel;
