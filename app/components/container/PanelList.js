import React,{Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	ScrollView,
	TextInput
} from 'react-native'

import {colors} from '../../settings';
import Button from '../commons/Button'
import ListItem from '../presentation/ListItem'

class PanelList extends Component {

	constructor(){
		super()
		this.state = {
			searchInput : ''
		}
	}

    _onPressBtn(){
		const {addListToPanel} = this.props;

    	let listName = this.state.searchInput;
    	if(undefined != listName && listName != ""){
    		addListToPanel(listName);
    		//reset state panel text
    		this.setState({
    			searchInput : ''
    		});

    		this._textInput.setNativeProps({text: ''});
    	}
    }

    _renderLists(){
		const {lists,post} = this.props;

    	if(undefined != lists && lists.length > 0){
    		return lists.map((item,i) => {
    			//obtener todos los posts de la lista
    			let posts = post.collection.filter((post) => post.assignedTo == item.id)
    			return (<ListItem
	    			onPress={() => {
						this._onSelectList(i);
					}}
	    			key={i}
	    			name={item.name}
	    			posts={posts.length}
	    			style={(lists[i].isSelected) ? styles.selectedList : null}
	    		/>)
	    	})
    	}
    }

    _onSelectList(i){
		const {unselectAllLists,selectList} = this.props;
    	unselectAllLists();
    	selectList(i);
    }

    render() {
        return (
        	<View style={styles.container}>
        		<View style={styles.header}>
        			<Text style={styles.headerTitle}>Listas del día</Text>
        		</View>

        		<View style={styles.subHeader}>
        			<TextInput
        				ref={component => this._textInput = component}
        				style={styles.searchInput}
        				placeholder="Nombre de la lista"
        				placeholderTextColor={colors.greyBold}
        				editable={true}
        				secureTextEntry={false}
        				onChangeText={(text) => this.setState({searchInput : text})}
        			/>
        			<Button
        				style={styles.btnSearch}
        				fontSize={14}
        				onPress={() => {
							this._onPressBtn();
						}}>
        				Agregar
        			</Button>
        		</View>

        		<ScrollView>
        			{this._renderLists()}
        		</ScrollView>
				
        	</View>
        );
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		backgroundColor : colors.white
	},
	subHeader : {
		backgroundColor : colors.grey,
		height : 50,
		alignItems : 'center',
		justifyContent : 'space-between',
		flexDirection : 'row'
	},
	header : {
		backgroundColor : colors.blueBold,
		height : 45,
		alignItems : 'center',
		justifyContent : 'center'
	},
	headerTitle : {
		color : colors.white,
		fontSize : 18
	},
	searchInput :{
		backgroundColor : colors.grey,
		borderBottomWidth : 2,
		borderBottomColor : colors.greyBold,
		color : colors.greyBold,
		flex : 1,
		paddingLeft : 20
	},
	btnSearch : {
		borderRadius : 50,
		paddingTop : 8,
		paddingBottom : 8,
		paddingLeft : 12,
		paddingRight : 12,
		shadowColor : colors.greyBold,
		shadowOpacity : 0.5,
		shadowOffset : {
			width : 3,
			height : 3
		},
		marginRight : 20
	},
	selectedList : {
		backgroundColor : 'rgba(231,246,251,.92)'
	}
})

export default PanelList;
