import React, {Component} from 'react';
import {
	StyleSheet,
	Text,
	View,
	Image,
	Dimensions,
	InteractionManager
} from 'react-native'

import {colors} from '../../settings';
import InputIcon from '../commons/InputIcon';
import Button from '../commons/Button';
import {NavPage} from '../';

class LoginPage extends Component {

	_onLoginPressed(){
		const {login,nav} = this.props;
		login({});
		InteractionManager.runAfterInteractions(() => {
			nav.push({
				component : NavPage
			});
		});
	}

    render() {
        return (
        	<View style={styles.container}>
        		<Image
		          source={require('../../assets/logo.png')}
		          style={styles.image}
		        />
		        <InputIcon placeholder="Correo Electrónico" id="email" type="text" icon="envelope-o" isPassword={false} />
		        <InputIcon placeholder="Contraseña" id="password" type="password" icon="unlock" isPassword={true} hasRightButton="eye" replaceRightButton="eye-slash" />
        		<Button
        			onPress={this._onLoginPressed.bind(this)}
        			style={styles.button}>
        			Ingresar
        		</Button>
        	</View>
        )
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		backgroundColor : colors.blue,
		justifyContent : 'center',
		alignItems : 'center',
		flexDirection : 'column'
	},
	image : {
		width : 250
	},
	button : {
		marginTop: 20,
		width : 250,
		borderRadius : 25,
		height : 50
	}
})

export default LoginPage;
