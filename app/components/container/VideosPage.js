import React,{Component} from 'react';
import {
	View,
	Text,
	ScrollView,
	StyleSheet,
	Modal,
	WebView
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../settings';
import VideoItem from '../presentation/VideoItem'
import {VideoPlayer} from '../../components';

class VideosPage extends Component {

	constructor(){
		super();
		this.state = {
			modalVisible : false,
			videoURL : ""
		};
	}

    render() {
        return (
        	<View style={styles.container}>
                <View style={styles.marginLeft}>
                    <Icon name="video-camera" size={35} color={colors.greyBold} />
                    <Text style={styles.title}>Videos</Text>
                </View>
        		<ScrollView
        			contentContainerStyle={styles.scrollView}>
        			{this._renderVideos()}
        		</ScrollView>


				<Modal
		          animationType={"slide"}
		          transparent={true}
		          visible={this.state.modalVisible}
		          onRequestClose={() => {}}
		          >

					<VideoPlayer
						onClose={() => {
							this._hideModal()
						}}
						uri={this.state.videoURL} />
				</Modal>

        	</View>
        )
    }

	_hideModal(){
		this.setState({
			modalVisible : false,
			videoURL : ""
		});
	}

	_showVideo(uri){
		this.setState({
			modalVisible : true,
			videoURL : uri
		});
	}

    _renderVideos(){
		const {pages,post} = this.props;
    	//obtener las fotos de la nota por medio de redux
    	let postID = pages.currentNote
    	let posts = post.collection.find((item) => item.id == postID)

    	return posts.videos.map((item,i) => {
			return (
				<VideoItem
					key={i}
					src={item.uri}
					name={item.name}
					id={item.id}
					onPress={() => {
						this._showVideo(item.uri)
					}}
				/>
			)
		});
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
        paddingTop : 20
	},
	scrollView : {
		padding : 20
	},
    marginLeft : {
        flexDirection : 'row',
        paddingLeft : 20,
        alignItems : 'center',
        justifyContent : 'flex-start',
		marginBottom : 20
    },
    title : {
        fontSize : 30,
        color : colors.greyBold,
        paddingLeft : 20,
    },
	webview : {
		width : 300,
		height : 400
	}
})

export default VideosPage;
