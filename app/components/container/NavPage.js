import React, {Component} from 'react';
import {
	View,
	Navigator,
    StyleSheet
} from 'react-native'

import {colors} from '../../settings';
import {DashboardPage} from '../';
//import Menu from '../presentation/Menu'
/*
	<Menu
		{...this.props}
	/>
*/
class NavPage extends Component {
    render() {
        return (
        	<View style={styles.container}>

        		<Navigator
        			configureScene={(route) => {
                        let animation = route.animation || 'FloatFromBottom';
                        return Navigator.SceneConfigs[animation];
                    }}
                    initialRoute={{name: 'DashboardPage', index: 0}}
                    ref="nav"
                    renderScene={this._renderScene.bind(this)}
        		/>

        	</View>
        )
    }

    _renderScene(route, navigator) {
        if (route.component) {
            return (
                <route.component
                    {...route.props}
                    {...this.props}
                    nav={navigator}
                    onBack={() => { navigator.pop() }} />
            );
        }

        return (
            <DashboardPage
                {...route.props}
                nav={navigator}
            />
        );

        /*
			onBack={() => {
                    if (route.index > 0)
                        navigator.pop()
                }}
        */
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		backgroundColor : colors.grey,
        flexDirection : 'column'
	}
})

export default NavPage;
