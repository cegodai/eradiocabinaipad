import React, {PropTypes} from 'react';
import {
    Dimensions,
    Image,
    ScrollView,
    StyleSheet
} from 'react-native';

const ImageZoom = (props) => {
    const {src,style} = props;

    return (
        <ScrollView
            centerContent={true}
            maximumZoomScale={3}
            minimumZoomScale={1}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{flex: 1}}>
            <Image
                resizeMode={'contain'}
                source={{uri: src}}
                style={[styles.image,style]} />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    image: {
        flex: 1
    }
});

export default ImageZoom;
