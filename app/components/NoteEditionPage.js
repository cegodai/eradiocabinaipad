import React,{Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../settings';
import {Utils} from '../helpers';
import {RichTextEditor, RichTextToolbar} from 'react-native-zss-rich-text-editor';

const ITEM_PADDING = 10;

export default class NoteEditionPage extends Component{
	render(){
		const {title,htmlContent,nav} = this.props;
		return (
				<View style={styles.container}>

					<View style={styles.header}>
						<TouchableOpacity
							activeOpacity={0.4}
							onPress={() => {
								nav.pop();
							}}
						>
							<Icon name="arrow-left" size={30} color={colors.white} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => {
							this._saveChanges();
						}}>
							<Text style={{fontSize:20,color:colors.white,paddingLeft : 25}}>Guardar edición</Text>
						</TouchableOpacity>
					</View>


					<View style={styles.editBar}>

						<View>
							<TouchableOpacity style={styles.titleSelector}>
								<Text style={{fontSize:17,paddingRight:10}}>Título</Text>
								<Icon name="sort-down" size={20} color={colors.greyBold} />
							</TouchableOpacity>
						</View>

						<View style={styles.textStyle}>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._setBold();
							}}>
								<Icon name="bold" size={20} color={colors.greyBold} />
							</TouchableOpacity>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._setItalic();
							}}>
								<Icon name="italic" size={20} color={colors.greyBold} />
							</TouchableOpacity>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._setUnderline();
							}}>
								<Icon name="underline" size={20} color={colors.greyBold} />
							</TouchableOpacity>
						</View>

						<View style={styles.alignTexts}>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._alignCenter();
							}}>
								<Icon name="align-center" size={20} color={colors.greyBold} />
							</TouchableOpacity>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._justify();
							}}>
								<Icon name="align-justify" size={20} color={colors.greyBold} />
							</TouchableOpacity>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._alignLeft();
							}}>
								<Icon name="align-left" size={20} color={colors.greyBold} />
							</TouchableOpacity>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._alignRight();
							}}>
								<Icon name="align-right" size={20} color={colors.greyBold} />
							</TouchableOpacity>
						</View>

						<View style={styles.listStyle}>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._ordererList();
							}}>
								<Icon name="list-ol" size={20} color={colors.greyBold} />
							</TouchableOpacity>
							<TouchableOpacity style={styles.btnPadding}
							onPress={() => {
								this._unordererList();
							}}>
								<Icon name="list-ul" size={20} color={colors.greyBold} />
							</TouchableOpacity>
						</View>

						<TouchableOpacity style={styles.btnPadding} onPress={() => {
							this._insertLink();
						}}>
							<Icon name="link" size={20} color={colors.greyBold} />
						</TouchableOpacity>

	        		</View>



					<RichTextEditor
					  ref={(r) => { this.richtext = r}}
					  initialTitleHTML={Utils.striptags(title)}
					  titlePlaceholder={"Escriba un título"}
					  contentPlaceholder={"Escriba aquí el contenido de la nota"}
					  initialContentHTML={htmlContent}
					  editorInitializedCallback={() => {
						  this.onEditorInitialized()
					  }}
					  setCustomCSS={"padding-top:20px;"}
					/>


				</View>
		);
	}

	_setBold(){
		this.richtext.setBold()
	}

	_setItalic(){
		this.richtext.setItalic()
	}

	_setUnderline(){
		this.richtext.setUnderline()
	}

	_alignLeft(){
		this.richtext.alignLeft()
	}

	_alignRight(){
		this.richtext.alignRight()
	}

	_alignCenter(){
		this.richtext.alignCenter()
	}

	_justify(){
		this.richtext.alignFull()
	}

	_unordererList(){
		this.richtext.insertBulletsList()
	}

	_ordererList(){
		this.richtext.insertOrderedList()
	}

	_insertLink(){
		this.richtext.showLinkDialog()
	}

	_updateLink(){
		this.richtext.updateLink()
	}

	_insertImage(){
		this.richText.insertImage({src: '*URL_OF_IMAGE*'});
	}

	async _saveChanges(){
		let content  = await this.richtext.getContentHtml();
		//alert(content)
	}
}

const styles = StyleSheet.create({
	container : {
		flex : 1
	},
	header : {
		flexDirection : "row",
		padding : 30,
		alignItems : "center",
		justifyContent : "space-between",
		backgroundColor : colors.blue,
		marginBottom : 35
	},
	textStyle : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between"
	},
	alignTexts : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between"
	},
	listStyle : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between"
	},
	btnPadding : {
		paddingLeft : ITEM_PADDING,
		paddingRight : ITEM_PADDING
	},
	titleSelector : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "space-between",
		paddingLeft : ITEM_PADDING,
		paddingRight : ITEM_PADDING
	},
	editBar : {
		backgroundColor : colors.semiGrey,
		padding : 20,
		flexDirection : 'row',
		alignItems : "center",
		justifyContent : "space-between",
		paddingLeft : 20,
		paddingRight : 20,
		position : "absolute",
		left :0,
		right : 0,
		top : 70
	}
});
