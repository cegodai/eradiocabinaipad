import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	TouchableOpacity
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../settings';

class InputIcon extends Component {

	constructor(props){
		super(props)
		this.state = {
			isPasswordVisible: !this.props.isPassword,
		    password: ''
		}
	}

	showPassword(){
		this.setState({
			isPasswordVisible: !this.state.isPasswordVisible
		})
	}

    render() {
    	const {placeholder,icon,hasRightButton,type,id,replaceRightButton} = this.props
    	const rightButtonIconClass = hasRightButton;
    	const rightButtonIconClassReplaced = replaceRightButton;

        return (
        	<View style={styles.wrapper}>
        		<View style={styles.container}>

        			<Icon name={icon} size={30} color={colors.white} style={styles.icon} />

        			{(() => {
        				if(this.state.isPasswordVisible){
        					return <TextInput ref={id} editable={true} placeholderTextColor={colors.white} placeholder={placeholder} secureTextEntry={false} style={styles.input} />
        				}else{
        					return <TextInput ref={id} editable={true} placeholderTextColor={colors.white} placeholder={placeholder} secureTextEntry={true}  style={styles.input} />
        				}
        			})()}
				    {(() => {
				        if(hasRightButton != undefined && replaceRightButton != undefined){
				        	if(type == 'password'){
								return <Icon name={this.state.isPasswordVisible ? rightButtonIconClass : rightButtonIconClassReplaced} size={30} color={colors.white} onPress={this.showPassword.bind(this)} style={styles.icon} />
				        	}else{
								return <Icon name={hasRightButton} size={30} color={colors.white} style={styles.icon} />
							}
				        }
				    })()}
				</View>
        	</View>
        );
    }
}

const styles = StyleSheet.create({
	wrapper : {
		flexDirection : 'row',
		marginTop : 25,
		borderBottomWidth : 2,
		borderBottomColor: colors.white,
		width : 250
	},
	container : {
		flexDirection : 'row',
		flex : 1,
		paddingBottom : 10
	},
	icon : {
    	opacity : 0.75
	},
	input : {
		color : colors.white,
		backgroundColor : 'transparent',
		paddingTop : 7,
		paddingRight: 7,
		paddingBottom: 7,
		paddingLeft : 20,
		fontSize : 15,
		flex : 1
	}
})

export default InputIcon;
