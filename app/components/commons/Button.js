import React,{Component} from 'react';
import {
	StyleSheet,
	Text,
    TouchableOpacity,
    View
} from 'react-native'

import {colors} from '../../settings';

class Button extends Component {
    render() {
        return (
        	<View>
        		<TouchableOpacity
                    activeOpacity={0.6}
                    onPress={this.props.onPress}
                    style={[styles.container, this.props.style]}>
                    <Text
                        style={[styles.text,(this.props.fontSize) ? {fontSize : this.props.fontSize} : styles.text ]}>
                        {this.props.children}
                    </Text>
                </TouchableOpacity>
        	</View>
        );
    }
}

const styles = StyleSheet.create({
	container: {
        alignItems: 'center',
        backgroundColor: colors.orange,
        justifyContent: 'center',
    },

    text: {
        color: colors.white,
        fontSize: 21,
        fontWeight: "bold"
    }
})

export default Button;
