import React,{Component} from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity,
	Dimensions
} from 'react-native'

const screen = Dimensions.get("window");
const LEFT_PANEL = screen.width / 3;
const MARGIN_ITEM = 6;
const ITEM_NUMBERS = 3;
const ITEM_WIDTH = (screen.width - LEFT_PANEL - (MARGIN_ITEM * 10)) / ITEM_NUMBERS;

class PhotoItem extends Component {
    render() {
    	const {src,name,id,onPress} = this.props
        return (
        	<TouchableOpacity
        		activeOpacity={0.5}
        		style={styles.container}
				onPress={onPress}
        		>
        		<Image
		          source={{uri : src}}
		          style={styles.container}
		        />
        	</TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
	container : {
		width : ITEM_WIDTH,
		height : ITEM_WIDTH,
		marginBottom : 10,
		marginRight : MARGIN_ITEM
	}
})

export default PhotoItem;
