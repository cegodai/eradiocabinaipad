import React from 'react'
import {WebView, View, Text} from "react-native";
import Dimensions from 'Dimensions';
import {colors} from '../../settings';

const deviceWidth = Dimensions.get('window').width;
const BODY_TAG_PATTERN = /\<\/ *body\>/;

var script = `
;(function() {
    var wrapper = document.createElement("div");
    wrapper.id = "height-wrapper";
    while (document.body.firstChild) {
        wrapper.appendChild(document.body.firstChild);
    }
    document.body.appendChild(wrapper);
    var i = 0;
    function updateHeight() {
        document.title = wrapper.clientHeight;
        window.location.hash = ++i;
    }
    updateHeight();
    window.addEventListener("load", function() {
        updateHeight();
        setTimeout(updateHeight, 1000);
    });
    window.addEventListener("resize", updateHeight);
}());
`;


const style = `
<style>
body, html, #height-wrapper {
    margin: 0;
    padding: 0 0 32px 0;
    font-family: sans-serif;
    font-size : 16px;
}

#height-wrapper {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
}

.content {
	font-family: sans-serif;
	font-size: 13px;
	color: ${colors.text};
	overflow-x: hidden;
	width: 100%;
},

.content iframe,
.content p iframe{
	height: ${deviceWidth/2}px !important;
	width: ${deviceWidth}px !important;
}

textarea{
	height: ${deviceWidth/2}px !important;
	width: ${deviceWidth}px !important;
	resize : none;
	border:0;
	font-family: sans-serif;
	font-size: 13px;
	color: ${colors.text};
}

img {
    height: auto !important;
    width: 100%;
}
</style>
<script>
${script}
</script>
`;

const codeInject = (html) => html.replace(BODY_TAG_PATTERN, style + "</body>");

var WebViewAutoHeight = React.createClass({

    propTypes: {
        source: React.PropTypes.object.isRequired,
        injectedJavaScript: React.PropTypes.string,
        minHeight: React.PropTypes.number,
        onNavigationStateChange: React.PropTypes.func,
        style: WebView.propTypes.style,
    },

    getDefaultProps() {
        return {minHeight: 100};
    },

    getInitialState() {
        return {
            realContentHeight: this.props.minHeight,
        };
    },

    handleNavigationChange(navState) {
        if (navState.title) {
            const realContentHeight = parseInt(navState.title, 10) || 0; // turn NaN to 0
            this.setState({realContentHeight});
        }
        if (typeof this.props.onNavigationStateChange === "function") {
            this.props.onNavigationStateChange(navState);
        }
    },

    render() {
        const {source, style, minHeight, ...otherProps} = this.props;
        const html = source.html;

        if (!html) {
            throw new Error("WebViewAutoHeight supports only source.html");
        }

        if (!BODY_TAG_PATTERN.test(html)) {
            throw new Error("Cannot find </body> from: " + html);
        }

        //console.log('wh',Math.max(this.state.realContentHeight, minHeight))

        return (
            <View style={{height: Math.max(this.state.realContentHeight, minHeight)}}>
                <WebView
                    {...otherProps}
                    source={{html: codeInject(html)}}
                    scrollEnabled={false}
                    style={[style, {height: Math.max(this.state.realContentHeight, minHeight)}]}
                    javaScriptEnabled
                    onNavigationStateChange={this.handleNavigationChange}
                />
            </View>
        );
    },

});


export default WebViewAutoHeight;
