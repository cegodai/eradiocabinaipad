import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Image,
	Dimensions
} from 'react-native';
import {colors} from '../../settings';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Utils} from '../../helpers';

const screen = Dimensions.get('window')
const ITEM_HEIGHT = screen.width / 5.7

export default PanelItem = (props) => (

    <TouchableOpacity
    	activeOpacity={0.4}
    	style={[styles.container,props.style]}
    	onPress={props.onPress}
    >
    	{(() => {
    		if(props.itemIndex == props.index){
    			return (
    				<View style={styles.overlay}>
			    		{props.children}
			    	</View>
    			)
    		}
    	})()}

    	{(() => {
    		if(props.assignedTo == ''){
    			return (
    				<View style={styles.reference}>
			    		<View style={styles.dot}></View>
			    		<Text style={styles.textReference}>No asignado</Text>
			    	</View>
    			)
    		}else{
    			return (
    				<View style={styles.reference}>
			    		<View style={styles.greydot}></View>
			    		<Text style={styles.textReference}>{props.assignedTo}</Text>
			    	</View>
    			)
    		}
    	})()}

    	<View style={styles.noteContainer}>

    		<Image
	          source={{uri : props.image}}
	          style={styles.image}
	        />
	        <View style={styles.noteContent}>
				<Text
		        	numberOfLines={2}
		        	style={styles.noteText}
		        >
		        	{Utils.striptags(props.desc)}
		        </Text>
		        <View style={styles.iconContainer}>
		        	<Icon name="microphone" size={15} color={colors.greyBold} style={styles.space} />
		        	<Icon name="video-camera" size={15} color={colors.greyBold} style={styles.space} />
		        	<Icon name="camera" size={15} color={colors.greyBold} style={styles.space} />
		        	<Icon name="pencil" size={15} color={colors.greyBold} />
	        	</View>
	        </View>
    	</View>


		<View style={styles.author}>
			<Image source={{uri:props.author.picture}}
				style={{
					width : 15,
					height : 15,
					borderRadius : 7
				}}
			/>
			<Text numberOfLines={1}
			style={{paddingLeft : 15}}>{props.author.name}</Text>
		</View>

    	</TouchableOpacity>
);

const styles = StyleSheet.create({
	container : {
		flexDirection : 'column',
		backgroundColor : colors.white,
		height : ITEM_HEIGHT,
		//alignItems : 'flex-start',
		borderBottomWidth : 2,
		borderBottomColor : colors.grey,
		padding : 20,
		position : 'relative'
	},
	greydot : {
		backgroundColor : colors.greyBold,
		borderRadius : 50,
		width : 15,
		height : 15
	},
	greytextReference : {
		marginLeft : 8,
		color:colors.grey,
		fontSize : 15
	},
	dot : {
		backgroundColor : colors.blue,
		borderRadius : 50,
		width : 15,
		height : 15
	},
	reference : {
		flexDirection : 'row'
	},
	textReference : {
		marginLeft : 8,
		color:colors.greyBold,
		fontSize : 15
	},
	noteContainer : {
		flex : 1,
		flexDirection : 'row',
		justifyContent : 'center',
		marginTop : 10
	},
	noteText : {
		color : colors.black,
		fontSize : 15,
		marginBottom : 18
	},
	image : {
		borderRadius : 10,
		height : 70,
		width : 70
	},
	noteContent : {
		flex : 1,
		flexDirection : 'column',
		paddingLeft : 20
	},
	space : {
		marginRight : 15
	},
	iconContainer : {
		flex : 1,
		flexDirection : 'row',
	},
	overlay : {
		backgroundColor : 'rgba(231,246,251,.92)',
		zIndex : 3,
		flex : 1,
		flexDirection : 'row',
		alignItems : 'center',
		justifyContent : 'space-around',
		//height : ITEM_HEIGHT,
		position: 'absolute',
		top:0,
		left:0,
		right:0,
		bottom:0
	},
	author : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "flex-end",
		paddingTop : 20,
		//flex : 1
	}
})
