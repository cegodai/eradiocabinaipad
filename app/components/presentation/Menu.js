import React, {Component} from 'react';
import {
	View,
	Text,
	TouchableOpacity,
	StyleSheet,
	Dimensions
} from 'react-native'

const screen = Dimensions.get('window')
const ITEM_WIDTH = screen.width
const ITEM_HEIGHT = ITEM_WIDTH / 5

import {colors} from '../../settings';
import Icon from 'react-native-vector-icons/FontAwesome';

class Menu extends Component {
    render() {
        return (
        	<View style={styles.container}>
        		<TouchableOpacity
        			onPress={this._onTapItem.bind(this)}
        			style={styles.itemMenu}
        		>
        			<Icon name="bank" size={50} color={colors.white} />
        			<Text style={styles.itemTextSize}>Dashboard</Text>
        		</TouchableOpacity>
        	</View>
        );
    }

    _onTapItem(){
    	console.log('taped!')
    }
}

const styles = StyleSheet.create({
	container :{
		flex : 1,
		width : 100,
		flexDirection : 'column',
		backgroundColor : colors.blue
	},
	itemMenu : {
		flex : 1,
		flexDirection : 'column',
		width : ITEM_WIDTH,
		height : ITEM_HEIGHT,
		alignItems : 'center',
		justifyContent : 'center'
	},
	itemTextSize : {
		color : colors.white,
		fontSize : 25,
		alignSelf : 'center'
	}
})

export default Menu;
