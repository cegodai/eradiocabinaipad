import React,{Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	ActivityIndicator
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../settings';

class AudioItem extends Component {
    render() {
    	const {name,duration,src,onPress,isLoading,index,indexPlaying} = this.props;
		//index => 3 iLoading => -1 indexPlaying => -1
        return (
        	<View style={styles.container}>
        		<View style={styles.text}>
        			<Text style={styles.title}>{name}</Text>
        			<Text style={styles.duration}>{duration}</Text>
        		</View>
        		<View style={styles.rows}>
					{isLoading === index && indexPlaying === -1 && (
						<ActivityIndicator
							size="small"
							style={styles.marginRight}
						/>
					)}

					{indexPlaying === index && isLoading == -1 && (
						<TouchableOpacity
	        			 activeOpacity={0.5}
	        			 style={styles.marginRight}
						 onPress={() =>{
							 onPress()
						 }}>
	        			 	<Icon name="pause" size={20} color={colors.blue} />
	        			</TouchableOpacity>
					)}

					{isLoading != index && indexPlaying != index && (
						<TouchableOpacity
	        			 activeOpacity={0.5}
	        			 style={styles.marginRight}
						 onPress={() =>{
							 onPress()
						 }}>
	        			 	<Icon name="play" size={20} color={colors.greyBold} />
	        			</TouchableOpacity>
					)}

        			<TouchableOpacity
        			 activeOpacity={0.5}>
        			 	<Icon name="trash" size={20} color={colors.greyBold} />
        			</TouchableOpacity>
        		</View>
        	</View>
        )
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		flexDirection : 'row',
		alignItems : 'center',
		justifyContent : 'space-between',
		paddingTop : 10,
		paddingBottom : 10,
		paddingLeft : 20,
		paddingRight : 20,
		borderWidth : 1,
		borderTopColor : 'transparent',
		borderLeftColor : 'transparent',
		borderRightColor : 'transparent',
		borderBottomColor : colors.grey
	},
	text : {
		flexDirection : 'column'
	},
	rows : {
		flexDirection : 'row'
	},
	title : {
		fontSize : 17
	},
	duration : {
		paddingTop : 5,
		fontSize : 15
	},
	marginRight : {
		paddingRight : 20
	}
})

export default AudioItem;
