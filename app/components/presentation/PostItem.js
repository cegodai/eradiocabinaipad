import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Image,
	Dimensions
} from 'react-native';
import {colors} from '../../settings';
import Icon from 'react-native-vector-icons/FontAwesome';

const screen = Dimensions.get('window')
const ITEM_HEIGHT = screen.width / 7

export default PostItem = (props) => (

    <TouchableOpacity
    	activeOpacity={0.4}
    	style={[styles.container,(props.currentPage === props.id) ? styles.selectedItem : {}]}
    	onPress={props.onPress}
    >
    	<View style={styles.noteContainer}>

    		<Image
	          source={{uri : props.image}}
	          style={styles.image}
	        />
	        <View style={styles.noteContent}>
	        	<Text
		        	numberOfLines={2}
		        	style={styles.noteText}
		        >
		        	{props.desc}
		        </Text>
		        <View style={styles.iconContainer}>
		        	<Icon name="microphone" size={15} color={colors.greyBold} style={styles.space} />
		        	<Icon name="video-camera" size={15} color={colors.greyBold} style={styles.space} />
		        	<Icon name="camera" size={15} color={colors.greyBold} style={styles.space} />
		        	<Icon name="pencil" size={15} color={colors.greyBold} />
	        	</View>
	        </View>
    	</View>


		<View style={styles.author}>
			<Image source={{uri:props.author.picture}}
				style={{
					width : 15,
					height : 15,
					borderRadius : 7
				}}
			/>
			<Text numberOfLines={1}
			style={{paddingLeft : 15}}>{props.author.name}</Text>
		</View>

    	</TouchableOpacity>
);

const styles = StyleSheet.create({
	container : {
		flexDirection : 'column',
		backgroundColor : colors.white,
		height : ITEM_HEIGHT,
		//alignItems : 'flex-start',
		borderBottomWidth : 2,
		borderBottomColor : colors.grey,
		padding : 20
	},
	greydot : {
		backgroundColor : colors.greyBold,
		borderRadius : 50,
		width : 15,
		height : 15
	},
	greytextReference : {
		marginLeft : 8,
		color:colors.grey,
		fontSize : 15
	},
	dot : {
		backgroundColor : colors.blue,
		borderRadius : 50,
		width : 15,
		height : 15
	},
	reference : {
		flexDirection : 'row'
	},
	textReference : {
		marginLeft : 8,
		color:colors.greyBold,
		fontSize : 15
	},
	noteContainer : {
		flex : 1,
		flexDirection : 'row',
		justifyContent : 'center',
	},
	noteText : {
		color : colors.black,
		fontSize : 15,
		marginBottom : 18
	},
	image : {
		borderRadius : 10,
		width : 100,
		height : 70,
		width : 70
	},
	noteContent : {
		flex : 1,
		flexDirection : 'column',
		paddingLeft : 20
	},
	space : {
		marginRight : 15
	},
	iconContainer : {
		flex : 1,
		flexDirection : 'row',
	},
	selectedItem : {
		backgroundColor : 'rgba(231,246,251,.92)'
	},
	author : {
		flexDirection : "row",
		alignItems : "center",
		justifyContent : "flex-end",
		paddingTop : 20,
		//flex : 1
	}
})
