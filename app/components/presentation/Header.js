import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Image
} from 'react-native';
import {colors} from '../../settings';
import Icon from 'react-native-vector-icons/FontAwesome';

export default Header = (props) => (
    <View style={styles.container}>
        <TouchableOpacity
        	activeOpacity={0.4}
        	style={styles.leftMargin}
        >
        	<Icon name="bars" size={20} color={colors.white} />
        </TouchableOpacity>
        <Image
          source={require('../../assets/logo.png')}
          style={styles.image}
		/>
		<TouchableOpacity
        	activeOpacity={0.4}
        >

        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
	container : {
		flexDirection : 'row',
		backgroundColor : colors.blue,
		alignItems : 'center',
		justifyContent : 'space-between',
		paddingTop : 25,
		paddingBottom : 20,
		borderWidth : 0
	},
	image  : {
		width : 166,
		height : 64.5,
		marginLeft : -40
	},
	leftMargin : {
		marginLeft : 20
	}
})
