import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	Dimensions
} from 'react-native'

const screen = Dimensions.get('window')

import {colors} from '../../settings';
import Icon from 'react-native-vector-icons/FontAwesome';

class ListItem extends Component {
    render() {
    	const {name,posts,onPress,style} = this.props
        return (
        	<View style={[styles.container,style]}>
        		<TouchableOpacity
        			activeOpacity={0.4}
        			style={styles.textLeft}
        			onPress={onPress}>
        			<Text style={styles.listTitle} numberOfLines={1}>{name}</Text>
        			<View style={styles.posts}>
        				<Icon name="newspaper-o" size={18} color={colors.greysemiLight} />
        				<Text style={styles.postLength} numberOfLines={1}>{posts} noticias</Text>
        			</View>
        		</TouchableOpacity>
        		<TouchableOpacity
        			activeOpacity={0.4}
        			style={styles.textRight}
        			onPress={this._onPressItemOpt.bind(this)}
        			>
        			<Icon name="ellipsis-v" size={30} color={colors.greysemiLight} />
        		</TouchableOpacity>
        	</View>
        );
    }

    _onPressItemOpt(){
    	console.log('pressed item option')
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		flexDirection : 'row',
		alignItems : 'center',
		justifyContent : 'space-between',
		padding : 20,
		borderWidth : 2,
		borderBottomColor : colors.grey,
		borderLeftColor : 'transparent',
		borderRightColor : 'transparent',
		borderTopColor : 'transparent',
		//width : screen.width / 6
	},
	textLeft : {
		flex : 1,
		flexDirection : 'column',
		alignItems : 'flex-start',
		justifyContent : 'center'
	},
	textRight : {
		alignItems : 'center',
		justifyContent : 'center',
		paddingLeft : 7,
		paddingRight : 7
	},
	postLength : {
		marginLeft : 10,
		fontSize : 16,
		color : colors.greysemiLight
	},
	listTitle : {
		fontSize : 20,
		color : colors.greysemiBold,
		textAlign : 'left',
		marginBottom : 10
	},
	posts : {
		flexDirection : 'row'
	}
})

export default ListItem;
