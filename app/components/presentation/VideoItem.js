import React,{Component} from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity,
	Dimensions
} from 'react-native'

import {colors} from '../../settings';
import {Icon} from '../';

const screen = Dimensions.get("window");

const LEFT_PANEL = screen.width / 3;
const VIDEO_PADDING = 20;
const VIDEO_WIDTH = screen.width - LEFT_PANEL - VIDEO_PADDING;

class VideoItem extends Component {
    render() {
    	const {src,name,id,onPress} = this.props
        return (
        	<View
        		style={styles.container}
        		>
		        <View style={styles.buttonWrapper}>
		        	<Image
			          source={{uri : src}}
			          style={styles.imgcontainer}
			        />

		        	<TouchableOpacity
		        		activeOpacity={0.5}
		        		style={styles.button}
						onPress={onPress}
		        	>
		        		<Icon style={styles.icon} name="play" size={60} color={colors.black} />
		        	</TouchableOpacity>

		        </View>
        	</View>
        );
    }
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		marginBottom : 30,
	},
	imgcontainer : {
		width : VIDEO_WIDTH,
		height : 320
	},
	buttonWrapper : {
		position : 'relative',
		height : 320,
		//width : VIDEO_WIDTH,
		alignItems : 'center',
		justifyContent : 'center',
	},
	button : {
		position : 'absolute',
		width : 100,
		height : 100,
		borderRadius : 100,
		padding : 25,
		zIndex : 9,
		top : 320 / 3,
		left : VIDEO_WIDTH / 2.3,
		backgroundColor : colors.white
	},
	icon : {
		marginLeft : 5,
		marginTop : -3
	}
})

export default VideoItem;
