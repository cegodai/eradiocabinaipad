import React from 'react';
import {
	View,
	WebView,
	StyleSheet,
	Text,
	Dimensions,
	ScrollView,
	Image,
	TouchableOpacity
} from 'react-native';

import {colors} from '../settings';
import {Utils} from '../helpers';
import {Icon,ViewPager} from '../components';
import ImageZoom from './ImageZoom';

const screen = Dimensions.get("window");
const IMAGE_PADDING = 50;
const ImageViewer = (props) => {
	//alert(props.index)
	const index = (props.index > -1) ? props.index : 0;
	return (
		<View style={styles.container}>

			<View style={styles.header}>
				<TouchableOpacity
					activeOpacity={0.4}
					onPress={() => {
						props.onClose();
					}}
				>
					<Icon name="times" size={30} color={colors.white} />
				</TouchableOpacity>
			</View>


			<ViewPager
				selectedIndex={index}
				style={{flex:1,borderColor:"transparent",borderWidth:1}}
			>
				{props.collection.length > 0 && props.collection.map((item,i) => {
					return (

						<View style={styles.imageContainer}
						key={i}>
							<ImageZoom

								src={item.uri}
								style={styles.image}
							/>
						</View>

					)
				})}
			</ViewPager>

		</View>
	);
};

const styles = StyleSheet.create({
	container  : {
		flex : 1,
		alignItems : "flex-start",
		justifyContent : "center",
		backgroundColor : "rgba("+Utils.hexToRgb(colors.blue).r+","+Utils.hexToRgb(colors.blue).g+","+Utils.hexToRgb(colors.blue).b+",0.8)"
	},
	image : {
		width : screen.width - IMAGE_PADDING *2 ,
		height : screen.height - IMAGE_PADDING *2,
		marginBottom : IMAGE_PADDING,
		marginLeft : IMAGE_PADDING,
		marginRight : IMAGE_PADDING
	},
	imageContainer : {
		flex:1,
		borderColor:"transparent",
		borderWidth : 1
	},
	scroll : {
		backgroundColor : "transparent"
	},
	header : {
		flexDirection : "row",
		padding : 30,
		alignItems : "center",
		justifyContent : "flex-start",
		backgroundColor : colors.blue
	}
});

export default ImageViewer;
