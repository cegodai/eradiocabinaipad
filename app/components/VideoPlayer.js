import React from 'react';
import {
	View,
	StyleSheet,
	WebView,
	TouchableOpacity,
	Dimensions
} from 'react-native';

import {Icon} from '../components';
import {colors} from '../settings';
import {Utils} from '../helpers';

const screen = Dimensions.get("window");
const IMAGE_PADDING = 50;

const VideoPlayer = (props) => {
	return(
		<View style={styles.container}>

			<View style={styles.header}>
				<TouchableOpacity
					activeOpacity={0.4}
					onPress={() => {
						props.onClose();
					}}
				>
					<Icon name="times" size={30} color={colors.white} />
				</TouchableOpacity>
			</View>

			<WebView
				source={{url:props.uri}}
				style={styles.webview}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	container : {
		flex : 1,
		backgroundColor : "rgba("+Utils.hexToRgb(colors.blue).r+","+Utils.hexToRgb(colors.blue).g+","+Utils.hexToRgb(colors.blue).b+",0.8)",
		alignItems : "flex-start",
		justifyContent : "center",
	},
	header : {
		flexDirection : "row",
		padding : 30,
		alignItems : "center",
		justifyContent : "flex-start",
		backgroundColor : colors.blue
	},
	webview : {
		width : screen.width - IMAGE_PADDING *2 ,
		height : screen.height - IMAGE_PADDING *2,
		marginBottom : IMAGE_PADDING,
		marginLeft : IMAGE_PADDING,
		marginRight : IMAGE_PADDING,
		backgroundColor : "transparent"
	}
});

export default VideoPlayer;
