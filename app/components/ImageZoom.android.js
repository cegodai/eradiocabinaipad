import React, {PropTypes} from 'react';
import {
    StyleSheet,
    WebView
} from 'react-native';

const htmlStringTemplate = `
<html>
<head>
<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    body {
        -webkit-align-items: center;
        align-items: center;
        display: -webkit-flex;
        display: flex;
        -webkit-justify-content: center;
        justify-content: center;
    }
    img {
        height: auto !important;
        width: 100%;
    }
</style>
</head>
<body>
<img src="IMAGE_SRC" />
</body>
</html>
`;

const ImageZoom = (props) => {
    const {src} = props;
    const html = htmlStringTemplate.replace('IMAGE_SRC', src);

    return (
        <WebView
            source={{html}}
            style={styles.webView}/>
    );
};

ImageZoom.propTypes = {
    src: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
    webView: {
        backgroundColor: 'transparent',
        flex: 1
    }
});

export default ImageZoom;
