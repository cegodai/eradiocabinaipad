import Icon from 'react-native-vector-icons/FontAwesome';
import LoginPage from '../providers/LoginProvider';
import NavPage from '../providers/NavPageProvider';
import DashboardPage from '../providers/DashBoardPageProvider';
import PanelList from '../providers/PanelListProvider';
import Panel from '../providers/PanelProvider';
import PanelPosts from '../providers/PanelPostProvider';

import VideoPlayer from './VideoPlayer';
import ImageViewer from './ImageViewer';
import PhotosPage from './container/PhotosPage';
import VideosPage from './container/VideosPage';
import AudiosPage from './container/AudiosPage';
import NoteContent from './container/NoteContent';

import NotePage from '../providers/NotePageProviders';

import ViewPager from './ViewPager';

export {
	VideoPlayer,
	ImageViewer,
	PanelList,
	DashboardPage,
	AudiosPage,
	Icon,
	LoginPage,
	NavPage,
	NoteContent,
	PhotosPage,
	VideosPage,
	ViewPager,
	NotePage,
	Panel,
	PanelPosts
};
